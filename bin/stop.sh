#!/bin/bash
source /etc/profile
path=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
cd $path
kill $(ps -ef | grep create-data-1.0.jar | grep -v grep | awk '{print $2}')